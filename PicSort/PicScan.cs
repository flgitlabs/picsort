using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Text;

using AlgerSoftware.Utils.Hashs;

namespace AlgerSoftware.Apps.PicSort
{
    public class PicScan
    {

        public void ScanPicDir(PicSort_DB picDB, string dirRoot, string dirRestart, string LogFile)
        {
            //  Create a directory reader to scan the picture directories
            //
            FSReader frdr = new FSReader(dirRoot, FSReaderModes.Recursive);
            //frdr.SetRestart(@"");

            //  Loop through the files returned by the FSReader
            //
            while (frdr.MoveNextFile())
            {
                uint crc = 0;
                int imgHeight = 0;
                int imgWidth = 0;
                Bitmap imgPic = null;
                string ExMsg = "";
                FileInfo myFile = frdr.CurrentFile;

                //  Only select the proper types of files
                //
                switch (myFile.Extension.ToLower())
                {
                    case ".jpg":
                        break;
                    case ".jpe":
                        break;
                    case ".jpeg":
                        break;
                    case ".png":
                        break;
                    default:
                        WriteLogMsg(LogFile, string.Format("Not Picture File: {0} {1}", myFile.FullName, ""));
                        continue;
                }

                //  Detremine if the file is already in the database
                //
                if (picDB.LookupPicture(frdr.CurrentDir.CurrentDir.FullName, frdr.CurrentFile.Name) != 0)
                {
                    continue;
                }

                //  Not in the database, open the image, collect information and store the info in the database
                //
                Stream fPic = myFile.OpenRead();                    // Open a file stream
                try
                {
                    imgPic = new Bitmap(fPic);                          // Read the image into a bitmap
                    imgHeight = imgPic.Size.Height;                     // Get the picture dimensions
                    imgWidth = imgPic.Size.Width;
                    crc = Crc32.GetStreamCRC32(fPic, 16384);            // Calc the file CRC32 value
                    //imgPic = new Bitmap(myFile.FullName);
                    picDB.SavePictureInfo(frdr.CurrentDir.CurrentDir.FullName,
                                            frdr.CurrentDir.CurrentDir.Name,
                                            frdr.CurrentFile.Name,
                                            imgHeight, imgWidth, crc, (int)frdr.CurrentFile.Length, 0);
                }
                catch (Exception ex)
                {
                    ExMsg = ex.Message;                                 // Capture the error type
                    WriteLogMsg(LogFile, string.Format("Load Error: {0} {1}", myFile.FullName, ExMsg));
                }
                fPic.Close();                                       // Now close the file
                //                txtout.WriteLine(string.Format("({0}:{1}) {2:X8} {3} {4}", imgHeight, imgWidth, crc, myFile.FullName, ExMsg));
            }

        }

        private void WriteLogMsg(string logFile, string logMsg)
        {
            StreamWriter txtout = new StreamWriter(logFile);
            txtout.WriteLine(logMsg);
            txtout.Close();
        }
    }
}
