using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace AlgerSoftware.Apps.PicSort
{
    public class PicSort_DB
    {
        //
        //  Class Private properties
        //
        private string dbConnStr;                   // Database connection string

        //
        //  Database access objects
        //
        SqlConnection dbConn;                       // Connection object

        #region Data Persistance Methods

        //
        //  S a v e P i c t u r e I n f o
        //
        //  Save the information on this picture.
        //
        public void SavePictureInfo(string dirPath, string dirName, string fileName, int height, int width, uint crcHash, int length, int error)
        {
            SqlCommand cmdSavPic = new SqlCommand("", dbConn);          // Create the command object
            SqlParameter prmSavPic;                                     // Now a parameter object

            cmdSavPic.CommandText = "dbo.usp_SavePictureInfo";
            cmdSavPic.CommandType = CommandType.StoredProcedure;        // Stored procedure to be called

            prmSavPic = new SqlParameter("@dirPath", SqlDbType.VarChar, 256);   // @dirPath - Directory path
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = dirPath;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@dirName", SqlDbType.VarChar, 256);   // @dirName - Directory name
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = dirName;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@fileName", SqlDbType.VarChar, 256);  // @fileName
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = fileName;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@height", SqlDbType.Int, 4);          // @height
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = height;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@width", SqlDbType.Int, 4);           // @width
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = width;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@crcHash", SqlDbType.Int, 4);         // @crcHash
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = (int)crcHash;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@length", SqlDbType.Int, 4);          // @length
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = length;
            cmdSavPic.Parameters.Add(prmSavPic);

            prmSavPic = new SqlParameter("@error", SqlDbType.Int, 4);           // @error
            prmSavPic.Direction = ParameterDirection.Input;
            prmSavPic.Value = error;
            cmdSavPic.Parameters.Add(prmSavPic);

            cmdSavPic.ExecuteNonQuery();                                        // Send the picture info to the database
        }

        //
        //   L o o k u p P i c t u r e
        //
        //  Search the database for the existance of the named picture.  Return its Picture table key.
        //
        public int LookupPicture(string dirPath, string fileName)
        {
            SqlCommand cmdSrchPic = new SqlCommand("", dbConn);         // Create the command object
            SqlParameter prmSrchPic;                                    // Now a parameter object

            cmdSrchPic.CommandText = "dbo.usp_LookupPicture";
            cmdSrchPic.CommandType = CommandType.StoredProcedure;       // Stored procedure to be called

            prmSrchPic = new SqlParameter("@dirPath", SqlDbType.VarChar, 256);   // @dirPath - Directory path
            prmSrchPic.Direction = ParameterDirection.Input;
            prmSrchPic.Value = dirPath;
            cmdSrchPic.Parameters.Add(prmSrchPic);

            prmSrchPic = new SqlParameter("@fileName", SqlDbType.VarChar, 256);  // @fileName
            prmSrchPic.Direction = ParameterDirection.Input;
            prmSrchPic.Value = fileName;
            cmdSrchPic.Parameters.Add(prmSrchPic);

            int PicKey = (int) cmdSrchPic.ExecuteScalar();              // Search for picture in the database

            return PicKey;  		

        }


        #endregion

        #region Open and Close
        //
        //  Methods
        //
        public void open()
        {
            if (dbConn == null)
                dbConn = new SqlConnection();               // Instantiate a new connection object
            if (dbConn.State != ConnectionState.Closed)
                throw new ApplicationException("dbOpen: Database is already open.");
            if (dbConnStr == null)
                throw new ApplicationException("dbOpen: null connection string.");
            dbConn.ConnectionString = dbConnStr;            // Use the saved connection string
            dbConn.Open();                                  // Now attempt to open a connaction the the database
        }


        public void close()
        {
            if (dbConn != null)                             // Must first be non-null
                if (dbConn.State != ConnectionState.Closed) // Already closed?
                    dbConn.Close();                         // Close the database connection
        }
        #endregion

        #region Constructors
        //
        //  Constructors
        //
        public PicSort_DB()
        {
        }

        public PicSort_DB(string ConnStr)
        {
            dbConnStr = ConnStr;                    // Save the string passed in the constructor
        }

        #endregion 
    }

}
