using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;


namespace AlgerSoftware.Apps.PicSort
{
    public partial class Form1 : Form
    {
        public PicSort_DB picDB;
        public string myRootDir = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PicSort.Properties.Settings mySettings = new AlgerSoftware.Apps.PicSort.Properties.Settings();
            string dbConStr = mySettings.DBConnStrSQLSVR;
            picDB = new PicSort_DB(dbConStr);
            picDB.open();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            picDB.close();
            Application.Exit();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void setPicDirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Select the current root dir
            //
            SetDirChooser.RootFolder = Environment.SpecialFolder.MyComputer;
            SetDirChooser.ShowDialog();
            string rootD = SetDirChooser.SelectedPath;  // Let the user select the picture root directory
            if (rootD != "")
            {
                myRootDir = rootD;
                toolStripStatusLabel1.Text = "Dir: " + rootD;
                startPicScanToolStripMenuItem.Enabled = true;
            }
            else
            {
                toolStripStatusLabel1.Text = "Dir: (none)";
                startPicScanToolStripMenuItem.Enabled = false;
            }
        }

        private void startPicScanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PicScan pScan = new PicScan();
            pScan.ScanPicDir(picDB, myRootDir, "", @"C:\Temp\PicSort5.log");
        }
    
    }
}