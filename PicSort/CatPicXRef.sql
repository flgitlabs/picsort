INSERT INTO CatPicXRef
                      (CPXPicKey, CPXCatKey, CPXPosition)
SELECT     PicCat.PicKey, Category.CatKey, 0 AS Expr1
FROM         Directory AS DirOrig INNER JOIN
                      Picture AS PicOrig ON DirOrig.DirKey = PicOrig.PicDirKey RIGHT OUTER JOIN
                      Picture AS PicCat ON PicOrig.PicHash = PicCat.PicHash AND PicOrig.PicFileName = PicCat.PicFileName RIGHT OUTER JOIN
                      Directory AS DirCat ON PicCat.PicDirKey = DirCat.DirKey AND DirOrig.DirKey <> DirCat.DirKey CROSS JOIN
                      Category
WHERE     (DirCat.DirPath = 'E:\System\tmp\Specials\Squeezed') AND (Category.CatName = 'asq')
ORDER BY DirOrig.DirPath