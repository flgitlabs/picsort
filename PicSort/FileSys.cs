using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AlgerSoftware.Apps.PicSort
{

    //
    //  Enumerations
    //
    public enum FSReaderModes
    {
        RootOnly = 0,                                   // File scan stays in the root directory
        Recursive                                       // Sub-directories are scanned, as well
    }

    //
    //  F S R e a d e r
    //
    //  Open the specified directory and provide navigation methods and access to directory and file 
    //  information.
    //
    public class FSReader
    {
        private FSReaderModes mReaderMode = FSReaderModes.RootOnly;      // Default stays in root directory
        private Stack<FSDirectory> mDirStack = new Stack<FSDirectory>(); // Directory travel stack
        private FSDirectory mDirRoot;                                   // The root directory for the search start

        public FSDirectory CurrentDir
        { get { return mDirStack.Peek(); } }                            // Return the current FSDirectory object
        
        public FileInfo CurrentFile
        { get { return CurrentDir.File;  } }                            // Return the current file in the enumerator

        public bool MoveNextFile()                                      // Move to the next file entry
        {
            if (mDirStack.Count == 0) return false;                     // Nothing more to return
            if (CurrentDir.MoveNextFile()) return true;                 // Return if a file is available
            if (mReaderMode != FSReaderModes.RootOnly)
            {
                if (CurrentDir.MoveNextDir())
                    mDirStack.Push(new FSDirectory(CurrentDir.SubDir));     // Put the next subdirectory on the stack
                else
                    mDirStack.Pop();                                        // All directories on this level have been returned
                return MoveNextFile();                                  // Continue the prior level
            }
            return false;                                               // No more files at this level
        }

        public void SetRestart(string filePath)
        {
            Stack<DirectoryInfo> rstStack = new Stack<DirectoryInfo>(); // Create an empty stack to trace back the start point
            FileInfo fEntry = new FileInfo(filePath);                   // The current file entry
            DirectoryInfo dirStep = fEntry.Directory;                   // Get the directory info for the bottom most directory 

            while (dirStep.FullName != mDirRoot.CurrentDir.FullName)
            {
                rstStack.Push(dirStep);                                 // Save the current directory entry on the stack
                dirStep = dirStep.Parent;                               // Go up one level
            }

            while (rstStack.Count > 0)                                  // Go through the stack and set up the FSReader position
            {
                while (CurrentDir.MoveNextDir())                            // Find the correct subdirectory
                {
                    if (CurrentDir.SubDir.Name == rstStack.Peek().Name)
                    {
                        while (CurrentDir.MoveNextFile())
                            ;
                        mDirStack.Push(new FSDirectory(rstStack.Pop()));  // Put the next subdirectory on the stack
                        break;
                    }
                }
            }

            while (MoveNextFile())                                      // On the end of the hierarchy, find the correct file in the list
            {
                if (CurrentFile.Name == fEntry.Name)
                    break;
            }
        }

        public FSReader(string dirPath, FSReaderModes fsMode)
        {
            mReaderMode = fsMode;                                       // Set the reader mode
            mDirRoot = new FSDirectory(dirPath);                        // Get the directory info entry for the reader start
            mDirStack.Push(mDirRoot);                                   // Push in the search root
        }
    }

    //
    //  F S D i r e c t o r y
    //
    //  Provide access to a specified directory and it's contained files.  Also, provide access to subdirectories
    //
    public class FSDirectory
    {
        //
        //  Class Private Information
        //
        private DirectoryInfo mCurrentDir;                              // Current directory for this object

        //  Directory list and enumerator
        // 
        private List<DirectoryInfo> mDirList;                           // Reference to our directory collection
        private List<DirectoryInfo>.Enumerator  mDirEnumerator;         // Directory list enumerator

        //  File List and enumerator
        //
        private List<FileInfo> mFileList;                               // List of FileInfo entries for this directory
        private List<FileInfo>.Enumerator  mFileEnumerator;             // File enumerator object
        private int mTotalFiles = 0;                                    // Number of files in this directory
        private int mFilesRemaining = 0;                                // Number of files yet to scan

        //
        //  Class Properties
        //
        public DirectoryInfo CurrentDir                                 // Return a reference to the current DirectoryInfo entry
            { get { return mCurrentDir; } }

        public FileInfo File
            { get { return mFileEnumerator.Current; } }                 // Return a reference to our FileInfo list

        public DirectoryInfo SubDir
            { get { return mDirEnumerator.Current; } }                  // Return the list of sub-directories

        public int TotalFiles
            { get { return mTotalFiles; } }                             // Get the count of directories

        public int FilesRemaining
            { get { return mFilesRemaining; } }                         // Number of files to scan

        //
        //  Class methods
        //
        protected void RetrieveSubDirList()
        {
            try
            {
                mDirList = new List<DirectoryInfo>(CurrentDir.GetDirectories()); // Retrieve the list of sub-directories
            }
            catch (Exception ex)
            {
                mDirList = new List<DirectoryInfo>();                           // Empty list if we hit an error
            }
            ResetDirEnumerator();                                       // Set up the enumerator
        }

        public void ResetDirEnumerator()
        {
            mDirEnumerator = mDirList.GetEnumerator();                  // Get the current File list enumerator
        }

        public bool MoveNextDir()
        {
            return mDirEnumerator.MoveNext();                           // Move to the next directory
        }

        protected void RetrieveFileList()
        {
            try
            {
                mFileList = new List<FileInfo>(CurrentDir.GetFiles());      // Get the list of files
                mFilesRemaining = mFileList.Count;
                mTotalFiles = mFileList.Count;
            }
            catch (Exception ex)
            {
                mFileList = new List<FileInfo>();                           // Create an empty list
            }
            ResetFileEnumerator();                                      // Set up the file enumerator
        }

        public void ResetFileEnumerator()
        {
            mFileEnumerator = mFileList.GetEnumerator();                // Get the current File list enumerator
        }

        public bool MoveNextFile()
        {
            mFilesRemaining--;
            return mFileEnumerator.MoveNext();                          // Move to the next file
        }

        //
        //  Class Constructors
        //
        public FSDirectory(DirectoryInfo dirInfo)                   // Directory specified by a DirectoryInfo object
        {
            mCurrentDir = dirInfo;                                      // Open the specified directory
            RetrieveSubDirList();                                       // Retrieve the subdirectory list
            RetrieveFileList();                                         // Retrieve the initial file list
        }

        public FSDirectory(string dirPath)
            : this(new DirectoryInfo(dirPath))                          // Directory path specified by a string
        {}
    }

}
