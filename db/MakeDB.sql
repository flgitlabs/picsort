IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CatPicXRef]') AND type in (N'U'))
DROP TABLE [dbo].[CatPicXRef]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Category]') AND type in (N'U'))
DROP TABLE [dbo].[Category]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Directory]') AND type in (N'U'))
DROP TABLE [dbo].[Directory]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Picture]') AND type in (N'U'))
DROP TABLE [dbo].[Picture]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Settings]') AND type in (N'U'))
DROP TABLE [dbo].[Settings]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateFileCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateFileCounts]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LookupPicture]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LookupPicture]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SavePictureInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SavePictureInfo]
GO

/****** Object:  Table [dbo].[Picture]    Script Date: 08/12/2006 21:45:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Picture](
	[PicKey] [int] IDENTITY(1,1) NOT NULL,
	[PicDirKey] [int] NOT NULL,
	[PicFileName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PicSize] [int] NOT NULL,
	[PicHSize] [int] NOT NULL,
	[PicVSize] [int] NOT NULL,
	[PicError] [int] NOT NULL DEFAULT (0),
	[PicHash] [int] NOT NULL,
	[PicDelete] [int] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_Picture] PRIMARY KEY CLUSTERED 
(
	[PicKey] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Category]    Script Date: 08/12/2006 21:45:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CatKey] [int] IDENTITY(1,1) NOT NULL,
	[CatParent] [int] NULL DEFAULT (0),
	[CatName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CatKey] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CatPicXRef]    Script Date: 08/12/2006 21:45:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatPicXRef](
	[CPXPicKey] [int] NOT NULL,
	[CPXCatKey] [int] NOT NULL,
	[CPXPosition] [int] NOT NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CatPicXRef]  WITH NOCHECK ADD  CONSTRAINT [FK_CatPicXRef_Category] FOREIGN KEY([CPXCatKey])
REFERENCES [dbo].[Category] ([CatKey])
GO
ALTER TABLE [dbo].[CatPicXRef] CHECK CONSTRAINT [FK_CatPicXRef_Category]
GO
ALTER TABLE [dbo].[CatPicXRef]  WITH NOCHECK ADD  CONSTRAINT [FK_CatPicXRef_Picture] FOREIGN KEY([CPXPicKey])
REFERENCES [dbo].[Picture] ([PicKey])
GO
ALTER TABLE [dbo].[CatPicXRef] CHECK CONSTRAINT [FK_CatPicXRef_Picture]
GO
/****** Object:  Table [dbo].[Directory]    Script Date: 08/12/2006 21:45:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Directory](
	[DirKey] [int] IDENTITY(1,1) NOT NULL,
	[DirParent] [int] NOT NULL DEFAULT (0),
	[DirName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DirPath] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DirFileCnt] [int] NOT NULL DEFAULT (0),
	[DirTotFileCnt] [int] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_Directory] PRIMARY KEY CLUSTERED 
(
	[DirKey] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Index [IX_DirName]    Script Date: 08/12/2006 21:45:52 ******/
CREATE NONCLUSTERED INDEX [IX_DirName] ON [dbo].[Directory] 
(
	[DirName] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_DirPath]    Script Date: 08/12/2006 21:45:52 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_DirPath] ON [dbo].[Directory] 
(
	[DirPath] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_PicDirKey]    Script Date: 08/12/2006 21:45:53 ******/
CREATE NONCLUSTERED INDEX [IX_PicDirKey] ON [dbo].[Picture] 
(
	[PicDirKey] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_PicFileName]    Script Date: 08/12/2006 21:45:53 ******/
CREATE NONCLUSTERED INDEX [IX_PicFileName] ON [dbo].[Picture] 
(
	[PicFileName] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

/****** Object:  Index [IX_PicHash]    Script Date: 08/12/2006 21:45:53 ******/
CREATE NONCLUSTERED INDEX [IX_PicHash] ON [dbo].[Picture] 
(
	[PicHash] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 08/12/2006 21:45:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings](
	[SetKey] [int] IDENTITY(1,1) NOT NULL,
	[SetName] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SetInt1] [int] NOT NULL,
	[SetInt2] [int] NOT NULL,
	[SetString1] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SetString2] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[SetKey] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[usp_LookupPicture]    Script Date: 08/12/2006 21:45:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.usp_LookupPicture
	(
	@dirPath	varchar(256),
	@fileName	varchar(256)
	)

AS
	
	DECLARE @PicKey	int
	
	SET @PicKey = 0 
	
	SELECT @PicKey = Pic.PicKey
		FROM dbo.Picture Pic
			INNER JOIN dbo.Directory Dir
				ON (Pic.PicDirKey = Dir.DirKey)
		WHERE (@dirPath = Dir.DirPath) AND (@fileName = Pic.PicFileName)
		
	SELECT @PicKey
	
	RETURN

GO
/****** Object:  StoredProcedure [dbo].[usp_SavePictureInfo]    Script Date: 08/12/2006 21:45:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.usp_SavePictureInfo
	(
	@dirPath	varchar(256),
	@dirName	varchar(256),
	@fileName	varchar(256),
	@height		int,
	@width		int,
	@crcHash	int,
	@length		int,
	@error		int
	)

AS
	DECLARE	@dirKey int
	DECLARE	@picKey	int
	
	SELECT @dirKey = DirKey
		FROM Directory
		WHERE (DirPath = @dirPath)
		
	IF (@@ROWCOUNT = 0)
	  BEGIN
		INSERT Directory (DirName, DirPath)
			VALUES (@dirName, @dirPath)
		SET @dirKey = @@IDENTITY
	  END
			
	SELECT @picKey = PicKey
		FROM Picture
		WHERE ((PicDirKey = @dirKey) AND (PicFileName = @fileName))
		
	IF (@@ROWCOUNT = 0)
		INSERT Picture (PicDirKey, PicFileName, PicSize, PicVSize, PicHSize, PicError, PicHash)
			VALUES (@dirKey, @fileName, @length, @height, @width, @error, @crcHash)
	
	RETURN

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateFileCounts]    Script Date: 08/12/2006 21:45:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE dbo.usp_UpdateFileCounts

AS
	SET NOCOUNT ON

	UPDATE Directory
		SET DirFileCnt = 0,
			DirTotFileCnt = 0

	UPDATE Directory
		SET DirFileCnt = PicTot.PicCnt,
			DirTotFileCnt = 0
		From Directory
			INNER JOIN (	SELECT Dir.DirKey, COUNT(Pic.PicKey) AS PicCnt
								FROM Directory Dir
									INNER JOIN Picture Pic
										ON (Dir.DirKey = Pic.PicDirKey)
								GROUP BY Dir.DirKey			
						) PicTot
				ON (Directory.DirKey = PicTot.DirKey)
	
	DECLARE	@DirKey	int
	DECLARE @DirParent	int
	DECLARE @DirFileCnt	int
	DECLARE @RCount		int
	DECLARE @Path		varchar(128)
	DECLARE @ParPath	varchar(128)
	
	DECLARE Pic_Cursor CURSOR FOR
			SELECT Dir.DirKey, Dir.DirParent, Dir.DirFileCnt, Dir.DirPath
				FROM Directory Dir
				WHERE (Dir.DirFileCnt > 0)
	
	OPEN Pic_Cursor
	
	FETCH NEXT FROM Pic_Cursor INTO @DirKey, @DirParent, @DirFileCnt, @Path
	WHILE @@FETCH_STATUS = 0
	  BEGIN
		---PRINT 'Fetch: ' + @Path
		WHILE 1=1
		  BEGIN
			UPDATE Directory
				SET DirTotFileCnt = DirTotFileCnt + @DirFileCnt
				FROM Directory
				WHERE (DirKey = @DirParent)
			SELECT @DirParent=DirParent,
				   @ParPath=DirPath
				FROM Directory
				WHERE (DirKey = @DirParent)
			---PRINT '  Updated: ' + @ParPath
			IF (@DirParent = 0) 
				BREAK
		  END
	  	FETCH NEXT FROM Pic_Cursor INTO @DirKey, @DirParent, @DirFileCnt, @Path
	  END
	
	CLOSE Pic_Cursor
	DEALLOCATE Pic_Cursor

	RETURN

GO
